pub mod handlers;
pub mod utils;

use std::{env, net::Ipv4Addr};

use actix_web::{
    middleware::Logger,
    web::{self, Data},
    App, HttpServer,
};

use anthill_di::DependencyContext;
use botx_api_framework::{
    botx_api::extensions::botx_api_context::IBotXApiContextExt,
    extensions::{
        anthill_di::IAnthillDiExt,
        actix::IActixHandlersExt
    }
};
use env_logger::Env;
use regex::Regex;

use handlers::{
    command::{
        echo::EchoCommandHandler,
        info::InfoCommandHandler,
        ping_bots::PingBotsCommandHandler,
        editable_message::EditableMessageCommandHandler,
        reply::ReplyCommandHandler,
        event_status::EventStatusCommandHandler,
        start_typing::StartTypingCommandHandler,
        stop_typing::StopTypingCommandHandler,
        upload_file::UploadFileCommandHandler,
        sticker_packs_list::StickerPacksListCommandHandler,
        new_sticker_pack::NewStickerPackCommandHandler,
        add_sticker::AddStickerCommandHandler,
        get_sticker_pack::GetStickerPackCommandHandler,
        get_sticker::GetStickerCommandHandler,
        update_sticker_pack::UpdateStickerPackCommandHandler,
        delete_sticker_pack::DeleteStickerPackCommandHandler,
        delete_sticker::DeleteStickerCommandHandler,
        download_file::DownloadFileCommandHandler,
        chats_list::ChatsListCommandHandler,
        chat_info::ChatInfoCommandHandler,
        add_user::AddUserCommandHandler,
        remove_user::RemoveUserCommandHandler,
        add_admin::AddAdminCommandHandler,
        set_stealth::SetStealthCommandHandler,
        disable_stealth::DisableStealthCommandHandler,
        create_chat::CreateChatCommandHandler,
        pin_message::PinMessageCommandHandler,
        unpin_message::UnpinMessageCommandHandler,
        get_users_by_email::GetUsersByEmailCommandHandler,
        get_user_by_huid::GetUserByHuidCommandHandler,
        get_user_by_login::GetUserByLoginCommandHandler,
        get_user_by_other_id::GetUserByOtherIdCommandHandler,
        get_users_as_csv::GetUsersAsCsvCommandHandler,
    },
    button::edit_message::EditMessageButtonHandler,
    message::MessageHandler
};

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    dotenv::dotenv().ok();
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let (ip, port) = get_address();

    log::info!("Start server: {ip}:{port}");

    let ioc_ctx = DependencyContext::new_root();

    ioc_ctx.register_botx_api_context().await?;
    ioc_ctx.register_botx_api_framework_context().await?;
    ioc_ctx.register_notification_context().await?;

    ioc_ctx.register_command_handler::<EchoCommandHandler>(Regex::new("^/echo|^#echo")?).await?;
    ioc_ctx.register_command_handler::<InfoCommandHandler>(Regex::new("^/help$|^/start$|^/info$|^#help$|^#start$|^#info$")?).await?;
    ioc_ctx.register_command_handler::<PingBotsCommandHandler>(Regex::new("^/ping_bots|^#ping_bots")?).await?;
    ioc_ctx.register_command_handler::<EditableMessageCommandHandler>(Regex::new("^/editable_message|^#editable_message")?).await?;
    ioc_ctx.register_command_handler::<ReplyCommandHandler>(Regex::new("^/reply|^#reply")?).await?;
    ioc_ctx.register_command_handler::<EventStatusCommandHandler>(Regex::new("^/event_status [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?|^#event_status [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$")?).await?;
    ioc_ctx.register_command_handler::<StartTypingCommandHandler>(Regex::new("^/start_typing$|^#start_typing$")?).await?;
    ioc_ctx.register_command_handler::<StopTypingCommandHandler>(Regex::new("^/stop_typing$|^#stop_typing$")?).await?;
    ioc_ctx.register_command_handler::<UploadFileCommandHandler>(Regex::new("^/upload_file|^#upload_file")?).await?;
    ioc_ctx.register_command_handler::<DownloadFileCommandHandler>(Regex::new("^/download_file [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [01]{1}$|^#download_file [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [01]{1}$")?).await?;
    ioc_ctx.register_command_handler::<StickerPacksListCommandHandler>(Regex::new("^/sticker_packs_list|^#sticker_packs_list")?).await?;
    ioc_ctx.register_command_handler::<NewStickerPackCommandHandler>(Regex::new("^/new_sticker_pack|^#new_sticker_pack")?).await?;
    ioc_ctx.register_command_handler::<AddStickerCommandHandler>(Regex::new("^/add_sticker [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? .+|^#add_sticker [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? .+")?).await?;
    ioc_ctx.register_command_handler::<GetStickerPackCommandHandler>(Regex::new("^/get_sticker_pack [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?|^#get_sticker_pack [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?")?).await?;
    ioc_ctx.register_command_handler::<GetStickerCommandHandler>(Regex::new("^/get_sticker [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?|^#get_sticker_pack [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?")?).await?;
    ioc_ctx.register_command_handler::<UpdateStickerPackCommandHandler>(Regex::new("^/update_sticker_pack [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? .+|^#update_sticker_pack [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? .+")?).await?;
    ioc_ctx.register_command_handler::<DeleteStickerPackCommandHandler>(Regex::new("^/delete_sticker_pack [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?|^#delete_sticker_pack [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?")?).await?;
    ioc_ctx.register_command_handler::<DeleteStickerCommandHandler>(Regex::new("^/delete_sticker [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?|^#delete_sticker [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?")?).await?;
    ioc_ctx.register_command_handler::<ChatsListCommandHandler>(Regex::new("^/chats_list$|^#chats_list$")?).await?;
    ioc_ctx.register_command_handler::<ChatInfoCommandHandler>(Regex::new("^/chat_info [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$|^#chat_info [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$")?).await?;
    ioc_ctx.register_command_handler::<AddUserCommandHandler>(Regex::new("^/add_user [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$|^#add_user [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$")?).await?;
    ioc_ctx.register_command_handler::<RemoveUserCommandHandler>(Regex::new("^/remove_user [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$|^#remove_user [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$")?).await?;
    ioc_ctx.register_command_handler::<AddAdminCommandHandler>(Regex::new("^/add_admin [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$|^#add_admin [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$")?).await?;
    ioc_ctx.register_command_handler::<SetStealthCommandHandler>(Regex::new("^/set_stealth [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [01]{1} [0-9]+ [0-9]+$|^#set_stealth [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [01]{1} [0-9]+ [0-9]+$")?).await?;
    ioc_ctx.register_command_handler::<DisableStealthCommandHandler>(Regex::new("^/disable_stealth [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$|^#set_stealth [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$")?).await?;
    ioc_ctx.register_command_handler::<CreateChatCommandHandler>(Regex::new("^/create_chat [a-zA-Zа-яА-ЯёЁ0-9_]+ [012]{1} [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [01]{1}|^#create_chat [a-zA-Zа-яА-ЯёЁ0-9]+ [012]{1} [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [01]{1}")?).await?;
    ioc_ctx.register_command_handler::<PinMessageCommandHandler>(Regex::new("^/pin_message [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$|^#pin_message [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]? [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$")?).await?;
    ioc_ctx.register_command_handler::<UnpinMessageCommandHandler>(Regex::new("^/unpin_message [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$|^#unpin_message [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$")?).await?;
    ioc_ctx.register_command_handler::<GetUsersByEmailCommandHandler>(Regex::new("^/get_users_by_email|^#get_users_by_email")?).await?;
    ioc_ctx.register_command_handler::<GetUserByHuidCommandHandler>(Regex::new("^/get_user_by_huid [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$|^#get_user_by_huid [{]?[0-9a-fA-F]{8}([0-9a-fA-F]{4}){3}[0-9a-fA-F]{12}[}]?$")?).await?;
    ioc_ctx.register_command_handler::<GetUserByLoginCommandHandler>(Regex::new("^/get_user_by_login|^#get_user_by_login")?).await?;
    ioc_ctx.register_command_handler::<GetUserByOtherIdCommandHandler>(Regex::new("^/get_user_by_other_id|^#get_user_by_other_id")?).await?;
    ioc_ctx.register_command_handler::<GetUsersAsCsvCommandHandler>(Regex::new("^/get_users_as_csv [01]{1} [01]{1} [01]{1}$|^#get_users_as_csv [01]{1} [01]{1} [01]{1}$")?).await?;
    ioc_ctx.register_message_handler::<MessageHandler>().await?;

    ioc_ctx.register_button_handler::<EditMessageButtonHandler>().await?;

    ioc_ctx.register_command_detection_regex(Regex::new("^/|^#").unwrap()).await?;

    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .app_data(web::PayloadConfig::new(usize::MAX))
            .app_data(web::JsonConfig::default().limit(usize::MAX))
            .app_data(Data::new(ioc_ctx.clone()))
            .add_botx_api_handlers()
    })
    .bind((ip.to_string(), port))?
    .run()
    .await?;

    Ok(())
}

const BOT_HOST_ENV: &str = "BOT_HOST";
const BOT_PORT_ENV: &str = "BOT_PORT";

fn get_address() -> (Ipv4Addr, u16) {
    let ip = env::var(BOT_HOST_ENV)
        .expect(&*format!("Env {BOT_HOST_ENV} not found"))
        .parse::<Ipv4Addr>()
        .expect(&*format!("Env {BOT_HOST_ENV} must be valid ipv4"));

    let port = env::var(BOT_PORT_ENV)
        .expect(&*format!("Env {BOT_PORT_ENV} not found"))
        .parse::<u16>()
        .expect(&*format!("Env {BOT_PORT_ENV} must be valid u32"));

    (ip, port)
}