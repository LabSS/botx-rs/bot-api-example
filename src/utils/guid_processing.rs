use std::sync::Arc;

use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        models::EventPayloadBuilder,
        v4::notification::direct::{
            models::DirectNotificationRequestBuilder,
            api::direct_notification
        },
        utils::auth_retry::retry_with_auth,
        context::BotXApiContext
    },
    contexts::RequestContext
};
use uuid::Uuid;

pub async fn parse_guid(api: &Arc<RwLock<BotXApiContext>>, guid: &str, request: &RequestContext) -> Option<Uuid> {
    match Uuid::parse_str(guid) {
        Ok(guid) => Some(guid),
        Err(err) => {
            let payload = EventPayloadBuilder::default()
                .with_body(format!("Ошибка формата uuid события: ```{:#?}```", err))
                .build()
                .expect("Не все поля нотификации были указаны");

            let direct_notification_request = DirectNotificationRequestBuilder::default()
                .with_group_chat_id(request.from.group_chat_id.unwrap())
                .with_notification(payload)
                .build()
                .expect("Не все поля запроса были указаны");

            retry_with_auth(api, || direct_notification(api, &direct_notification_request)).await.expect("Не удалось отправить сообщение");

            return None;
        }
    }
}