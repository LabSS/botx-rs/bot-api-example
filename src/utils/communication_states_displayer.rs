use std::{fmt::Debug, sync::Arc};

use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        v4::notification::direct::{
            models::DirectNotificationRequestBuilder,
            api::direct_notification
        },
        utils::auth_retry::retry_with_auth,
        models::EventPayloadBuilder,
        context::BotXApiContext
    },
    contexts::RequestContext
};

pub async fn send_full_request_response<TRequest: Debug, TResponse: Debug>(command_name: &str, api: &Arc<RwLock<BotXApiContext>>, request: &RequestContext, request_to_express: &TRequest, result_from_express: &TResponse) {
    let notification = EventPayloadBuilder::default()
        .with_body(format!("{command_name}\n\nПришло событие:
        ```{request:#?}```"))
        .build()
        .expect("Не все поля нотификации были указаны");

    let mut direct_notification_request = DirectNotificationRequestBuilder::default()
        .with_group_chat_id(request.from.group_chat_id.unwrap())
        .with_notification(notification)
        .build()
        .expect("Не все поля запроса были указаны");

    retry_with_auth(api, || direct_notification(api, &direct_notification_request)).await.expect("Не удалось отправить сообщение");

    direct_notification_request.notification.body = format!("Отправили событие в eXpress:
    ```{:#?}```", request_to_express);

    retry_with_auth(api, || direct_notification(api, &direct_notification_request)).await.expect("Не удалось отправить сообщение");

    direct_notification_request.notification.body = format!("Ответ express:
    ```{:#?}```", result_from_express);

    retry_with_auth(api, || direct_notification(api, &direct_notification_request)).await.expect("Не удалось отправить сообщение");
}

pub async fn send_request_response<TResponse: Debug>(command_name: &str, api: &Arc<RwLock<BotXApiContext>>, request: &RequestContext, result_from_express: &TResponse) {
    let notification = EventPayloadBuilder::default()
        .with_body(format!("{command_name}\n\nПришло событие:
        ```{request:#?}```"))
        .build()
        .expect("Не все поля нотификации были указаны");

    let mut direct_notification_request = DirectNotificationRequestBuilder::default()
        .with_group_chat_id(request.from.group_chat_id.unwrap())
        .with_notification(notification)
        .build()
        .expect("Не все поля запроса были указаны");

    retry_with_auth(api, || direct_notification(api, &direct_notification_request)).await.expect("Не удалось отправить сообщение");

    direct_notification_request.notification.body = format!("Сообщение к eXpress скрыто т.к. превышает лимиты размера сообщения пользователю");

    retry_with_auth(api, || direct_notification(api, &direct_notification_request)).await.expect("Не удалось отправить сообщение");

    direct_notification_request.notification.body = format!("Ответ express:
    ```{:#?}```", result_from_express);

    retry_with_auth(api, || direct_notification(api, &direct_notification_request)).await.expect("Не удалось отправить сообщение");
}


pub async fn send_response<TResponse: Debug>(command_name: &str, api: &Arc<RwLock<BotXApiContext>>, request: &RequestContext, result_from_express: &TResponse) {
    let notification = EventPayloadBuilder::default()
        .with_body(format!("{command_name}\n\nЗарос пользователя и запрос к eXpress скрыты, т.к. содержат слишком большой текст\n\nОтвет express:
        ```{:#?}```", result_from_express))
        .build()
        .expect("Не все поля нотификации были указаны");

    let direct_notification_request = DirectNotificationRequestBuilder::default()
        .with_group_chat_id(request.from.group_chat_id.unwrap())
        .with_notification(notification)
        .build()
        .expect("Не все поля запроса были указаны");

    retry_with_auth(api, || direct_notification(api, &direct_notification_request)).await.expect("Не удалось отправить сообщение");
}