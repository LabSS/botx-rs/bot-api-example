use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::users::by_other_id::api::get_user_by_other_id,
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct GetUserByOtherIdCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for GetUserByOtherIdCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let other_id = command[21..].trim().to_string();

        let get_user_by_other_id_result = retry_with_auth(&self.api, || get_user_by_other_id(&self.api, &other_id)).await;

        send_full_request_response("Запрос информации о пользователе по дополнительному id", &self.api, &request, &(other_id), &get_user_by_other_id_result).await;

        Ok(CommandOk::default())
    }
}
