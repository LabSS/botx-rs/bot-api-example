use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        models::EventPayloadBuilder,
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::events::reply_event::{
            api::reply_event,
            models::ReplyMessageRequestBuilder
        }
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct ReplyCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for ReplyCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let message = command[6..].trim().to_string();

        let payload = EventPayloadBuilder::default()
            .with_body(message)
            .build()
            .expect("Не все поля нотификации были указаны");

        let reply_request = ReplyMessageRequestBuilder::default()
            .with_source_sync_id(request.sync_id)
            .with_reply(payload)
            .build()
            .expect("Не все поля запроса были указаны");

        let reply_result = retry_with_auth(&self.api, || reply_event(&self.api, &reply_request)).await;

        send_full_request_response("Реакция на отправленное сообщение", &self.api, &request, &reply_request, &reply_result).await;

        Ok(CommandOk::default())
    }
}
