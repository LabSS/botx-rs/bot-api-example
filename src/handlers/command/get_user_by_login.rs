use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::users::by_login::api::get_user_by_login,
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct GetUserByLoginCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for GetUserByLoginCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let user_ad_params = command[18..].trim().to_string();

        let ad_params = user_ad_params.split(" ").map(|x| x.to_string()).collect::<Vec<String>>();

        let ad_login = ad_params[0].clone();
        let ad_domain = ad_params.get(1).map(|x| x.clone()).unwrap_or("".to_string());

        let get_user_by_login_result = retry_with_auth(&self.api, || get_user_by_login(&self.api, &ad_login, &ad_domain)).await;

        send_full_request_response("Запрос информации о пользователе по ad login + ad domain", &self.api, &request, &(ad_login, ad_domain), &get_user_by_login_result).await;

        Ok(CommandOk::default())
    }
}
