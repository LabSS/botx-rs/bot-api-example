use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::files::upload_file::{
            api::upload_file,
            models::{UploadRequestBuilder, FileMeta},
        },
    },
    contexts::RequestContext,
    handlers::command::ICommandHandler,
    results::*
};
use chrono::*;

use crate::utils::*;

#[derive(constructor)]
pub struct UploadFileCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for UploadFileCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let file_data = command[12..].trim().to_string();

        let now = Utc::now();

        let upload_file_request = UploadRequestBuilder::default()
            .with_group_chat_id(request.from.group_chat_id.unwrap())
            .with_content(file_data.as_bytes().to_vec())
            .with_file_name(format!("uploaded_file_{:02}_{:02}_{:02}.txt", now.hour(), now.minute(), now.second()))
            .with_meta(FileMeta {
                captions: Some("Подпись для загруженного файла".to_string()),
                duration: None
            })
            .with_mime_type("text/plain")
            .build()
            .expect("Не все поля запроса были указаны");

        let upload_file_response = retry_with_auth(&self.api, || upload_file(&self.api, &upload_file_request)).await;

        send_full_request_response("Загрузка файла. Зарос в multipart, по этому фактический запрос отличается от представленного", 
            &self.api, &request, &upload_file_request, &upload_file_response).await;

        Ok(CommandOk::default())
    }
}
