use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::users::by_huid::api::get_user_by_huid,
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct GetUserByHuidCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for GetUserByHuidCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let user_huid = command[17..].trim().to_string();

        let api = &self.api;

        let Some(user_huid) = parse_guid(api, &*user_huid, &request).await else {
            return Ok(CommandOk::default());
        };

        let get_user_by_huid_result = retry_with_auth(&self.api, || get_user_by_huid(&self.api, &user_huid)).await;

        send_full_request_response("Запрос информации о пользователе по email", &self.api, &request, &(user_huid), &get_user_by_huid_result).await;

        Ok(CommandOk::default())
    }
}
