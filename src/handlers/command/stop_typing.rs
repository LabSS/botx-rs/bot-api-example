use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::events::stop_typing::{
            api::stop_typing,
            models::StopTypingRequestBuilder
        },
    },
    contexts::RequestContext,
    handlers::command::ICommandHandler,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct StopTypingCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for StopTypingCommandHandler {
    async fn handle(&mut self, _command: String, request: RequestContext) -> CommandResult {
        let typing_request = StopTypingRequestBuilder::default()
            .with_group_chat_id(request.from.group_chat_id.unwrap())
            .build()
            .expect("Не все поля запроса были указаны");

        let typing_result = retry_with_auth(&self.api, || stop_typing(&self.api, &typing_request)).await;

        send_full_request_response("Окончание печати ботом", &self.api, &request, &typing_request, &typing_result).await;

        Ok(CommandOk::default())
    }
}
