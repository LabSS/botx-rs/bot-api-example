use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::stickers::sticker_packs_list::{
            api::sticker_packs_list,
            models::StickerPacksListRequestBuilder
        },
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct StickerPacksListCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for StickerPacksListCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let pagination_token = command[19..].trim().to_string();

        let mut builder = StickerPacksListRequestBuilder::default();
            //.with_user_huid(request.from.user_huid.map(|x| x.clone()).unwrap())
        builder.with_limit(2u32);

        if pagination_token.len() > 0 {
            builder.with_after(pagination_token);
        }

        let sticker_packs_list_request = builder.build()
            .expect("Не все поля были указаны");

        let sticker_packs_list_result = retry_with_auth(&self.api, || sticker_packs_list(&self.api, &sticker_packs_list_request)).await;

        send_full_request_response("Запрос списка наборов стикеров", &self.api, &request, &sticker_packs_list_request, &sticker_packs_list_result).await;

        Ok(CommandOk::default())
    }
}
