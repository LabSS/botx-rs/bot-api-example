use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::stickers::new_sticker_pack::{
            api::new_sticker_pack,
            models::NewStickerPackRequestBuilder
        }
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct NewStickerPackCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for NewStickerPackCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let command = command[17..].trim().to_string();

        let new_sticker_pack_request = NewStickerPackRequestBuilder::default()
            .with_name(command)
            .with_user_huid(request.from.user_huid.map(|x| x.clone()).unwrap())
            .build()
            .expect("Не все поля были указаны");

        let new_sticker_pack_result = retry_with_auth(&self.api, || new_sticker_pack(&self.api, &new_sticker_pack_request)).await;

        send_full_request_response("Создание набора стикеров", &self.api, &request, &new_sticker_pack_request, &new_sticker_pack_result).await;

        Ok(CommandOk::default())
    }
}
