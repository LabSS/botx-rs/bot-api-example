use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::events::status::api::event_status
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct EventStatusCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for EventStatusCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let event_uuid = command[13..].trim().to_string();

        let api = &self.api;

        let Some(event_uuid) = parse_guid(api, &*event_uuid, &request).await else {
            return Ok(CommandOk::default());
        };

        let event_status_result = retry_with_auth(&self.api, || event_status(&self.api, &event_uuid)).await;

        send_full_request_response("Запрос состояния события", &self.api, &request, &event_uuid, &event_status_result).await;

        Ok(CommandOk::default())
    }
}
