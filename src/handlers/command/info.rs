use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::context::BotXApiContext,
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::{utils::*, handlers::shared::send_info_message};

#[derive(constructor)]
pub struct InfoCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for InfoCommandHandler {
    async fn handle(&mut self, _command: String, request: RequestContext) -> CommandResult {
        let (direct_notification_request, direct_notification_result) = send_info_message(&self.api, &request).await;

        send_request_response("Запрос информации о боте", &self.api, &request, &direct_notification_result).await;

        Ok(CommandOk::default())
    }
}
