use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::chats::add_user::{api::add_user, models::AddUserRequestBuilder},
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct AddUserCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for AddUserCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let params = command[9..].trim().to_string();

        let params = params.split(" ").map(|x| x.to_string()).collect::<Vec<String>>();

        let api = &self.api;

        let Some(chat_id) = parse_guid(api, &*params[0], &request).await else {
            return Ok(CommandOk::default());
        };

        let Some(user_huid) = parse_guid(api, &*params[1], &request).await else {
            return Ok(CommandOk::default());
        };

        let add_user_request = AddUserRequestBuilder::default()
            .with_group_chat_id(chat_id)
            .with_user_huids(vec![user_huid])
            .build()
            .expect("Не все поля запроса были указаны");

        let add_user_result = retry_with_auth(&self.api, || add_user(&self.api, &add_user_request)).await;

        send_full_request_response("Добавление пользователя в чат бота", &self.api, &request, &add_user_request, &add_user_result).await;

        Ok(CommandOk::default())
    }
}
