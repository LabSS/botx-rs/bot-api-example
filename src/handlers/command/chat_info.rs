use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::chats::info::api::get_chat_info,
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct ChatInfoCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for ChatInfoCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let chat_id = command[10..].trim().to_string();

        let api = &self.api;

        let Some(chat_id) = parse_guid(api, &*chat_id, &request).await else {
            return Ok(CommandOk::default());
        };

        let sticker_packs_list_result = retry_with_auth(&self.api, || get_chat_info(&self.api, &chat_id)).await;

        send_full_request_response("Запрос информации о чате бота", &self.api, &request, &(chat_id), &sticker_packs_list_result).await;

        Ok(CommandOk::default())
    }
}
