use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::{api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::chats::create::{api::create_chat, models::CreateChatRequestBuilder},
    }, bot::models::{ChatType, Attachment}},
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct CreateChatCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for CreateChatCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let params = command[12..].trim().to_string();

        let params = params.split(" ").map(|x| x.to_string()).collect::<Vec<String>>();

        let name = params[0].clone();
        let chat_type = match params[1].parse::<u8>().expect("Неверный числовой формат chat_type") {
            1u8 => ChatType::GroupChat,
            2u8 => ChatType::Channel,
            _ => ChatType::Chat,
        };

        let api = &self.api;

        let Some(member) = parse_guid(api, &*params[2], &request).await else {
            return Ok(CommandOk::default());
        };

        let shared_history = if params[3] == "1" { true } else { false };

        let description = if params.len() > 4 { Some(params[4..].join(" ")) } else { None };

        let avatar = match request.attachments.first() {
            Some(Attachment::Image(avatar)) => Some(avatar.content.clone()),
            _ => None,
        };

        let mut create_chat_request_builder = CreateChatRequestBuilder::default();

        create_chat_request_builder.with_name(name);

        if let Some(description) = description {
            create_chat_request_builder.with_description(description);
        }

        create_chat_request_builder.with_chat_type(chat_type);

        create_chat_request_builder.with_members(vec![member]);

        if let Some(avatar) = &avatar {
            create_chat_request_builder.with_avatar(avatar);
        }

        create_chat_request_builder.with_shared_history(shared_history);
            
        let create_chat_request = create_chat_request_builder.build()
            .expect("Не все поля запроса были указаны");

        let create_chat_result = retry_with_auth(&self.api, || create_chat(&self.api, &create_chat_request)).await;

        if avatar.is_some() {
            send_response(&*format!("Создание чата с аватаркой"), &self.api, &request, &create_chat_result).await;
        } else {
            send_full_request_response(&*format!("Создание чата без аватарки"), &self.api, &request, &create_chat_request, &create_chat_result).await;
        }

        Ok(CommandOk::default())
    }
}
