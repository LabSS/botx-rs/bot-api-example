use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        models::*,
        context::BotXApiContext,
        v4::notification::direct::{
            api::direct_notification,
            models::DirectNotificationRequestBuilder,
        },
        utils::auth_retry::retry_with_auth
    },
    handlers::command::ICommandHandler,
    contexts::*,
    results::*
};

use crate::{
    handlers::button::edit_message::*,
    utils::*
};

#[derive(constructor)]
pub struct EditableMessageCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for EditableMessageCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let command = command[17..].trim().to_string();

        let notification = EventPayloadBuilder::default()
            .with_body(command.clone())
            .with_bubble(vec![vec![
                BubbleBuilder::default()
                    .with_label("Редактировать")
                    .with_command(format!("/edit_message"))
                    .with_data(EditMessageButtonData{ type_id: Default::default(), message: "Отредактированное сообщение".to_string() })
                    .build()
                    .expect("Не все поля кнопки были указаны")
            ]])
            .with_metadata(EditMessageButtonMetaData { type_id: Default::default() })
            .build()
            .expect("Не все поля нотификации были указаны");

        let direct_notification_request = DirectNotificationRequestBuilder::default()
            .with_group_chat_id(request.from.group_chat_id.unwrap())
            .with_notification(notification)
            .build()
            .expect("Не все поля запроса были указаны");

        let direct_notification_response = retry_with_auth(&self.api, || direct_notification(&self.api, &direct_notification_request)).await;

        // let callback_channel = self.notification_context.write().await.set_callback(response.result.sync_id.unwrap());
        // callback_channel.await;

        send_full_request_response("Отправка редактируемого сообщения", &self.api, &request, &direct_notification_request, &direct_notification_response).await;

        Ok(CommandOk::default())
    }
}
