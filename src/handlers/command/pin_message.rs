use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::chats::pin_message::{api::pin_message, models::PinMessageRequestBuilder},
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct PinMessageCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for PinMessageCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let params = command[12..].trim().to_string();

        let params = params.split(" ").map(|x| x.to_string()).collect::<Vec<String>>();

        let api = &self.api;

        let Some(chat_id) = parse_guid(api, &*params[0], &request).await else {
            return Ok(CommandOk::default());
        };

        let Some(message_id) = parse_guid(api, &*params[1], &request).await else {
            return Ok(CommandOk::default());
        };

        let pin_message_request = PinMessageRequestBuilder::default()
            .with_chat_id(chat_id)
            .with_sync_id(message_id)
            .build()
            .expect("Не все поля запроса были указаны");

        let pin_message_result = retry_with_auth(&self.api, || pin_message(&self.api, &pin_message_request)).await;

        send_full_request_response("Закрепление сообщения в чате бота", &self.api, &request, &pin_message_request, &pin_message_result).await;

        Ok(CommandOk::default())
    }
}
