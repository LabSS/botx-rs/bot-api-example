use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::stickers::update_sticker_pack::{
            api::update_sticker_pack,
            models::UpdateStickerPackRequestBuilder
        }
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct UpdateStickerPackCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for UpdateStickerPackCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let args = command[20..].trim().to_string();

        let args = args.split(" ").map(|x| x.to_string()).collect::<Vec<String>>();

        let api = &self.api;

        let Some(pack_id) = parse_guid(api, &*args[0], &request).await else {
            return Ok(CommandOk::default());
        };

        let Some(sticker_id) = parse_guid(api, &*args[1], &request).await else {
            return Ok(CommandOk::default());
        };

        let update_sticker_pack_request = UpdateStickerPackRequestBuilder::default()
            .with_name(args[2..].join(" "))
            .with_preview(sticker_id)
            .build()
            .expect("Не все поля были указаны");

        let update_sticker_pack_result = retry_with_auth(&self.api, || update_sticker_pack(&self.api, &pack_id, &update_sticker_pack_request)).await;

        send_full_request_response("Обновление набора стикеров", &self.api, &request, &(pack_id, update_sticker_pack_request), &update_sticker_pack_result).await;

        Ok(CommandOk::default())
    }
}
