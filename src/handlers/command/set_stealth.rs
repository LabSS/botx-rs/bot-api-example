use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::chats::set_stealth::{api::set_stealth, models::SetStealthRequestBuilder},
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct SetStealthCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for SetStealthCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let params = command[12..].trim().to_string();

        let params = params.split(" ").map(|x| x.to_string()).collect::<Vec<String>>();

        let api = &self.api;

        let Some(chat_id) = parse_guid(api, &*params[0], &request).await else {
            return Ok(CommandOk::default());
        };

        let mut set_stealth_request_builder = SetStealthRequestBuilder::default();

        set_stealth_request_builder.with_group_chat_id(chat_id);

        set_stealth_request_builder.with_disable_web(if params[1] == "1" { true } else { false });

        if params[2] != "0" {
            set_stealth_request_builder.with_burn_in(params[2].parse::<u32>().expect("Не верный числовой формат burn_in"));
        }

        if params[3] != "0" {
            set_stealth_request_builder.with_expire_in(params[3].parse::<u32>().expect("Не верный числовой формат expire_in"));
        }

        let set_stealth_request = set_stealth_request_builder.build()
            .expect("Не все поля запроса были указаны");

        let set_stealth_result = retry_with_auth(&self.api, || set_stealth(&self.api, &set_stealth_request)).await;

        send_full_request_response("Настройка и включение стелс режима бота", &self.api, &request, &set_stealth_request, &set_stealth_result).await;

        Ok(CommandOk::default())
    }
}
