use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        models::EventPayloadBuilder,
        context::BotXApiContext,
        v4::notification::direct::{
            api::direct_notification,
            models::DirectNotificationRequestBuilder,
        },
        utils::auth_retry::retry_with_auth
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::send_full_request_response;

#[derive(constructor)]
pub struct EchoCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for EchoCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let command = command[5..].trim().to_string();

        let notification = EventPayloadBuilder::default()
            .with_body(command)
            .build()
            .expect("Не все поля нотификации были указаны");

        let direct_notification_request = DirectNotificationRequestBuilder::default()
            .with_group_chat_id(request.from.group_chat_id.unwrap())
            .with_notification(notification)
            .build()
            .expect("Не все поля запроса были указаны");

        let direct_notification_result = retry_with_auth(&self.api, || direct_notification(&self.api, &direct_notification_request)).await;

        send_full_request_response("Команда эхо", &self.api, &request, &direct_notification_request, &direct_notification_result).await;

        Ok(CommandOk::default())
    }
}
