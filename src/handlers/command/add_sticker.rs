use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::{
        api::{
            models::EventPayloadBuilder,
            context::BotXApiContext,
            v4::notification::direct::{
                api::direct_notification,
                models::DirectNotificationRequestBuilder,
            },
            utils::auth_retry::retry_with_auth,
            v3::stickers::add_sticker::{
                api::add_sticker,
                models::AddStickerRequestBuilder
            }
        },
        bot::models::Attachment
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct AddStickerCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for AddStickerCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let args = command[12..].trim().to_string();

        let args = args.split(" ").map(|x| x.to_string()).collect::<Vec<String>>();

        let image = match request.attachments.first() {
            Some(Attachment::Image(image)) => image.clone(),
            _ => {
                let notification = EventPayloadBuilder::default()
                    .with_body("Приложите файл изображения в сообщение")
                    .build()
                    .expect("Не все поля нотификации были указаны");

                let direct_notification_request = DirectNotificationRequestBuilder::default()
                    .with_group_chat_id(request.from.group_chat_id.unwrap())
                    .with_notification(notification)
                    .build()
                    .expect("Не все поля запроса были указаны");

                retry_with_auth(&self.api, || direct_notification(&self.api, &direct_notification_request)).await.expect("Не удалось отправить сообщение");

                return Ok(CommandOk::default());
            }
        };
        
        let api = &self.api;

        let Some(pack_id) = parse_guid(api, &*args[0].clone(), &request).await else {
            return Ok(CommandOk::default());
        };

        let add_sticker_request = AddStickerRequestBuilder::default()
            .with_emoji(args[1].chars().nth(0).unwrap().clone())
            .with_image(image.content.clone())
            .build()
            .expect("Не все поля были указаны");

        let add_sticker_result = retry_with_auth(&self.api, || add_sticker(&self.api, &pack_id, &add_sticker_request)).await;

        send_response("Добавление стикера", &self.api, &request, &add_sticker_result).await;

        Ok(CommandOk::default())
    }
}
