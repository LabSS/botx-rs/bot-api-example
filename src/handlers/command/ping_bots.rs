use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        v4::notification::internal::{
            api::internal_notification,
            models::*
        },
        utils::auth_retry::retry_with_auth
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};
use serde::Serialize;

use crate::utils::*;

#[derive(Serialize, Clone, Debug)]
pub struct PingBotsCommandData {
    command: String,
}

#[derive(Serialize, Clone, Debug)]
pub struct PingBotsCommandOption {

}

#[derive(constructor)]
pub struct PingBotsCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for PingBotsCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let command = command[10..].trim().to_string();

        let internal_notification_request: InternalNotificationRequest<PingBotsCommandData, PingBotsCommandOption> = InternalNotificationRequestBuilder::default()
            .with_group_chat_id(request.from.group_chat_id.unwrap())
            .with_data(PingBotsCommandData { command })
            .with_opts(PingBotsCommandOption { })
            .build()
            .expect("Не все поля запроса были указаны");

        let internal_notification_result = retry_with_auth(&self.api, || internal_notification(&self.api, &internal_notification_request)).await;

        send_full_request_response("Отправка пинг сообщения всем ботам в чате", &self.api, &request, &internal_notification_request, &internal_notification_result).await;

        Ok(CommandOk::default())
    }
}
