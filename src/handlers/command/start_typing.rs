use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::events::typing::{
            api::start_typing,
            models::StartTypingRequestBuilder
        },
    },
    contexts::RequestContext,
    handlers::command::ICommandHandler,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct StartTypingCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for StartTypingCommandHandler {
    async fn handle(&mut self, _command: String, request: RequestContext) -> CommandResult {
        let start_typing_request = StartTypingRequestBuilder::default()
            .with_group_chat_id(request.from.group_chat_id.unwrap())
            .build()
            .expect("Не все поля запроса были указаны");

        let start_typing_result = retry_with_auth(&self.api, || start_typing(&self.api, &start_typing_request)).await;

        send_full_request_response("Начало печати ботом", &self.api, &request, &start_typing_request, &start_typing_result).await;

        Ok(CommandOk::default())
    }
}
