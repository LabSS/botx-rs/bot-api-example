use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::chats::unpin_message::{api::unpin_message, models::UnpinMessageRequestBuilder},
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct UnpinMessageCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for UnpinMessageCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let param = command[14..].trim().to_string();

        let api = &self.api;

        let Some(chat_id) = parse_guid(api, &*param, &request).await else {
            return Ok(CommandOk::default());
        };

        let unpin_message_request = UnpinMessageRequestBuilder::default()
            .with_chat_id(chat_id)
            .build()
            .expect("Не все поля запроса были указаны");

        let unpin_message_result = retry_with_auth(&self.api, || unpin_message(&self.api, &unpin_message_request)).await;

        send_full_request_response("Открепление сообщения в чате бота", &self.api, &request, &unpin_message_request, &unpin_message_result).await;

        Ok(CommandOk::default())
    }
}
