use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::stickers::get_sticker_pack::api::get_sticker_pack,
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct GetStickerPackCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for GetStickerPackCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let sticker_pack_id = command[17..].trim().to_string();

        let api = &self.api;

        let Some(pack_id) = parse_guid(api, &*sticker_pack_id, &request).await else {
            return Ok(CommandOk::default());
        };

        let get_sticker_pack_result = retry_with_auth(&self.api, || get_sticker_pack(&self.api, &pack_id)).await;

        send_full_request_response("Запрос информации о наборе стикеров", &self.api, &request, &pack_id, &get_sticker_pack_result).await;

        Ok(CommandOk::default())
    }
}
