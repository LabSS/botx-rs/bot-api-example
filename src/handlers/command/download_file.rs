use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::files::download_file::{
            api::download_file,
            models::DownloadFileRequestBuilder
        }
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct DownloadFileCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for DownloadFileCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let file_id_with_is_private = command[14..].trim().to_string();

        let args = file_id_with_is_private.split(" ")
            .map(|x| x.to_string())
            .collect::<Vec<String>>();

        let api = &self.api;

        let Some(file_id) = parse_guid(api, &*args[0], &request).await else {
            return Ok(CommandOk::default());
        };

        let download_file_request = DownloadFileRequestBuilder::default()
            .with_file_id(file_id)
            .with_group_chat_id(request.from.group_chat_id.map(|x| x.clone()).unwrap())
            .with_is_preview(if args[1] == "1" { true } else { false })
            .build()
            .expect("Не все поля запроса были указаны");

        let download_file_result = retry_with_auth(&self.api, || download_file(&self.api, &download_file_request)).await;

        send_full_request_response("Скачивание файла", &self.api, &request, &download_file_request, &download_file_result).await;

        Ok(CommandOk::default())
    }
}
