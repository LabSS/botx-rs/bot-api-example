use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::users::by_email::{api::get_users_by_email, models::GetUsersByEmailRequestBuilder},
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct GetUsersByEmailCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for GetUsersByEmailCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let params = command[19..].trim().to_string();

        let user_emails = params.split(" ").map(|x| x.to_string()).collect::<Vec<String>>();

        let get_users_by_email_request = GetUsersByEmailRequestBuilder::default()
            .with_emails(user_emails)
            .build()
            .expect("Не все поля запроса были указаны");

        let get_users_by_email_result = retry_with_auth(&self.api, || get_users_by_email(&self.api, &get_users_by_email_request)).await;

        send_full_request_response("Запрос информации о пользователе по email", &self.api, &request, &get_users_by_email_request, &get_users_by_email_result).await;

        Ok(CommandOk::default())
    }
}
