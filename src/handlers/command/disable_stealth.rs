use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::chats::disable_stealth::{api::disable_stealth, models::DisableStealthRequestBuilder},
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct DisableStealthCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for DisableStealthCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let chat_id = command[16..].trim().to_string();

        let api = &self.api;

        let Some(chat_id) = parse_guid(api, &*chat_id, &request).await else {
            return Ok(CommandOk::default());
        };

        let disable_stealth_request = DisableStealthRequestBuilder::default()
            .with_group_chat_id(chat_id)
            .build()
            .expect("Не все поля запроса были указаны");

        let disable_stealth_result = retry_with_auth(&self.api, || disable_stealth(&self.api, &disable_stealth_request)).await;

        send_full_request_response("Отключение стелс режима бота", &self.api, &request, &disable_stealth_request, &disable_stealth_result).await;

        Ok(CommandOk::default())
    }
}
