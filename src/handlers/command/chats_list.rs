use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::chats::list::api::get_chats_list,
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct ChatsListCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for ChatsListCommandHandler {
    async fn handle(&mut self, _command: String, request: RequestContext) -> CommandResult {
        let sticker_packs_list_result = retry_with_auth(&self.api, || get_chats_list(&self.api)).await;

        send_full_request_response("Запрос списка чатов бота", &self.api, &request, &(), &sticker_packs_list_result).await;

        Ok(CommandOk::default())
    }
}
