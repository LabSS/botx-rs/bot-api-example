use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::stickers::get_sticker::api::get_sticker,
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct GetStickerCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for GetStickerCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let sticker_pack_id_and_sticker_id = command[12..].trim().to_string();
        
        let ids = sticker_pack_id_and_sticker_id.split(" ").map(|x| x.to_string()).collect::<Vec<String>>();

        let api = &self.api;

        let Some(pack_id) = parse_guid(api, &*ids[0], &request).await else {
            return Ok(CommandOk::default());
        };

        let Some(sticker_id) = parse_guid(api, &*ids[1], &request).await else {
            return Ok(CommandOk::default());
        };

        let get_sticker_result = retry_with_auth(&self.api, || get_sticker(&self.api, &pack_id, &sticker_id)).await;

        send_full_request_response("Запрос информации о стикере", &self.api, &request, &(pack_id, sticker_id), &get_sticker_result).await;

        Ok(CommandOk::default())
    }
}
