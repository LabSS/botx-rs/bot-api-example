use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::users::users_as_csv::api::get_users_as_csv,
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct GetUsersAsCsvCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for GetUsersAsCsvCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let params = command[17..].trim().to_string();

        let params: Vec<u8> = params.split(" ").map(|x| x.to_string().parse::<u8>().unwrap()).collect::<Vec<u8>>();

        let cts_user = params[0] == 1;
        let unregistered = params[1] == 1;
        let botx = params[2] == 1;

        let get_users_as_csv_result = retry_with_auth(&self.api, || get_users_as_csv(&self.api, cts_user, unregistered, botx)).await;

        send_full_request_response("Запрос списка пользователей", &self.api, &request, &(cts_user, unregistered, botx), &get_users_as_csv_result).await;

        Ok(CommandOk::default())
    }
}
