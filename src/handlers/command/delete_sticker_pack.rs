use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth,
        v3::stickers::delete_sticker_pack::api::delete_sticker_pack,
    },
    handlers::command::ICommandHandler,
    contexts::RequestContext,
    results::*
};

use crate::utils::*;

#[derive(constructor)]
pub struct DeleteStickerPackCommandHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for DeleteStickerPackCommandHandler {
    async fn handle(&mut self, command: String, request: RequestContext) -> CommandResult {
        let pack_id = command[20..].trim().to_string();
                
        let api = &self.api;

        let Some(pack_id) = parse_guid(api, &*pack_id, &request).await else {
            return Ok(CommandOk::default());
        };

        let delete_sticker_pack_result = retry_with_auth(&self.api, || delete_sticker_pack(&self.api, &pack_id)).await;

        send_full_request_response("Удаление набора стикеров", &self.api, &request, &pack_id, &delete_sticker_pack_result).await;

        Ok(CommandOk::default())
    }
}
