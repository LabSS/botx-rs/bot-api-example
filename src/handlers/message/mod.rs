use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::context::BotXApiContext,
    handlers::message::IMessageHandler,
    results::*,
    contexts::RequestContext
};

use crate::{
    handlers::shared::send_info_message,
    utils::send_full_request_response
};

#[derive(constructor)]
pub struct MessageHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl IMessageHandler for MessageHandler {
    async fn handle(&mut self, _message: String, request_context: RequestContext) -> CommandResult {
        let (direct_notification_request, direct_notification_result) = send_info_message(&self.api, &request_context).await;

        send_full_request_response("Простое сообщение", &self.api, &request_context, &direct_notification_request, &direct_notification_result).await;

        Ok(CommandOk::default())
    }
}