use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        v3::events::edit_event::{
            api::edit_event,
            models::EditMessageRequestBuilder
        },
        models::EventPayloadBuilder,
        utils::auth_retry::retry_with_auth
    },
    button_data,
    results::*,
    handlers::button::IButtonHandler,
    contexts::RequestContext
};

use crate::utils::*;

#[derive(constructor)]
pub struct EditMessageButtonHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[button_data]
#[derive(Default)]
pub struct EditMessageButtonData {
    pub message: String,
}

#[button_data]
#[derive(Default)]
pub struct EditMessageButtonMetaData {}

#[async_trait_with_sync::async_trait]
impl IButtonHandler for EditMessageButtonHandler {
    type TData = EditMessageButtonData;
    type TMetaData = EditMessageButtonMetaData;

    async fn handle(&mut self, _button_text: String, data: Self::TData, _metadata: Self::TMetaData, request: RequestContext) -> CommandResult {
        let payload = EventPayloadBuilder::default()
            .with_body(data.message)
            .with_metadata(EditMessageButtonMetaData::default())
            .with_bubble(vec![vec![]])
            .build()
            .expect("Не все поля нотификации были указаны");

        let edit_message_request = EditMessageRequestBuilder::default()
            .with_sync_id(request.source_sync_id.unwrap())
            .with_payload(payload)
            .build()
            .expect("Не все поля запроса были указаны");

        let edit_message_result = retry_with_auth(&self.api, || edit_event(&self.api, &edit_message_request)).await;

        send_full_request_response("Событие редактирования сообщения", &self.api, &request, &edit_message_request, &edit_message_result).await;

        Ok(CommandOk::default())
    }
}