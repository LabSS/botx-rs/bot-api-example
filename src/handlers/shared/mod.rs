use std::sync::Arc;

use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        v4::notification::direct::{
            models::*,
            api::direct_notification
        },
        utils::auth_retry::retry_with_auth,
        result::BotXApiError,
        models::EventPayloadBuilder,
        context::BotXApiContext
    },
    contexts::RequestContext
};

pub async fn send_info_message(api: &Arc<RwLock<BotXApiContext>>, request: &RequestContext) -> (DirectNotificationRequest, Result<DirectNotificationResponse, BotXApiError<DirectNotificationExpressError>>) {
    let message = "Бот предоставляет обзор методов botx api
    
    Доступны следующие команды:
    `/start` или `/help` или `/info` - вывод списка команд
    `/echo <text>` - отправка эхо сообщения
    `/ping_bots <command>` - отправка команды всем ботам в чате
    `/editable_message <текст редактируемого сообщения>` - отправка сообщения с кнопкой редактирования
    `/reply <message>` - команда реакции на команду с заданным текстом
    `/event_status <event_uuid>` - запрос состояния события. uuid события можно посмотреть в выводе команды echo
    `/start_typing` - установка состояние печати сообщения ботом
    `/stop_typing` - отмена состояние печати сообщения ботом
    `/upload_file` - загрузка файла в чат
    `/download_file <id файла> <1/0 загрузка превью или полной версии>` - скачивание файла из чата
    `/sticker_packs_list <токен пагинации (опционально)>` - запрос списка наборов стикеров (пагинация по 10 шт)
    `/new_sticker_pack <название пака стикеров>` - создать новый набор стикеров
    `/add_sticker <id набора стикеров> <эмодзи стикера>` - добавить новый стикер в набор стикеров. при отправке команды требуется приложить к сообщению изображение стикера
    `/get_sticker_pack <id набора стикеров>` - запрос информации о наборе стикеров
    `/get_sticker <id набора стикеров> <id стикера>` - запрос информации о стикере
    `/update_sticker_pack <id набора стикеров> <id стикера для превью> <название набора стикеров>` - обновление набора стикеров
    `/delete_sticker_pack <id набора стикеров>` - удаление набора стикеров
    `/delete_sticker <id набора стикеров> <id стикера>` - удаление стикера из набора стикеров
    `/chats_list` - запрос списка чатов бота
    `/chat_info <id чата>` - запрос информации о чате бота
    `/add_user <id чата> <huid пользователя>` - добавление пользователя в чат бота
    `/remove_user <id чата> <huid пользователя>` - удаление пользователя из чата бота
    `/add_admin <id чата> <huid пользователя>` - добавление админа в чат бота
    `/set_stealth <id чата> <0/1 отключить доступ к чату с веб версии> <время до сгорания сообщения для прочитавшего, если 0 то отключено> <время до сгорания сообщения для всех участников чата, если 0 то отключено>` - включение и настройка стелс режима в чате
    `/disable_stealth <id чата>` - отключение стелс режима в чате
    `/create_chat <название без пробелов> <тип чата 0 - личный чат/1 - групповой чат/2 - канал> <id участника чата> <0/1 общая история> <описание, если пусто - без описания>` - создать чат, если необходим аватар приложите изображение к команде
    `/pin_message <id чата> <id сообщения>` - закрепление сообщения в чате
    `/unpin_message <id чата>` - открепление сообщения в чате
    `/get_users_by_email <несколько email через пробел>` - запрос информации о пользователях по email
    `/get_user_by_huid <huid пользователя>` - запрос информации о пользователе по huid
    `/get_user_by_login <ad login> <ad domain опционально>` - запрос информации о пользователе по ad login + ad domain
    `/get_user_by_other_id <дополнительный id пользователя>` - запрос информации о пользователе по дополнительному id пользователя
    `/get_users_as_csv <0/1 - включить в результат пользователей с типом cts_user> <0/1 - включить в результат пользователей с типом unregistered> <0/1 - включить в результат пользователей с типом botx>` - запрос списка пользователей
    
    Допустимо `/` заменить на `#`
    
    * Если вы сомневаетесь в поведении api можете воспользоваться этим ботом
    * Если вы не смогли воспроизвести свой кейс, откройте issue c описанием желаемых доработок
    ".to_string();

    let notification = EventPayloadBuilder::default()
        .with_body(message)
        .build()
        .expect("Не все поля нотификации были указаны");

    let direct_notification_request = DirectNotificationRequestBuilder::default()
        .with_group_chat_id(request.from.group_chat_id.unwrap())
        .with_notification(notification)
        .build()
        .expect("Не все поля запроса были указаны");

    let direct_notification_result = retry_with_auth(api, || direct_notification(api, &direct_notification_request)).await;

    (direct_notification_request, direct_notification_result)
}