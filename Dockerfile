FROM rust:1.67

WORKDIR /usr/src/bot-api-example
COPY . .

RUN rustup default nightly

RUN cargo install --path .

CMD ["bot-api-example"]